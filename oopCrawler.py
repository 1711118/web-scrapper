import requests
from bs4 import BeautifulSoup
import mysql.connector


class newEggCrawler:
    def __init__(self, url):
        self.url = url
        self.r = requests.get(self.url)
        self.soup = BeautifulSoup(self.r.content, 'html5lib')
        self.x = 1
        self.one_item_data = []

    def sendRequest(self):
        return requests.get(self.url)

    def makingSoup(self):
        return BeautifulSoup(self.r.content, 'html5lib')

    def lastButton(self):
        button = self.soup.findAll(
            "button", {"class": "btn"})

        buttons_list = []
        for each in button:
            buttons_list.append(str(each.contents))

        main_button = []
        for each in buttons_list:
            splitted = each.split("['")
            if(len(splitted)) == 2:
                for each in splitted:
                    final = each.split("']")
                    if final[0].isdigit() == True:
                        data = int(final[0])
                        type(data)
                        main_button.append(data)

        main_button.sort()
        return main_button[len(main_button)-1]

    def all_items(self):
        return self.soup.findAll(
            "div", {"class": "item-cell"})

    def crawl_to_product_link(self):
        links = []
        list_items = self.all_items()
        for item in list_items:
            data = item.find("a")
            # link for the product details
            product_link = str(data['href'])
            image = item.find("img")
            product_name = str(image["alt"])
            links.append(product_link + "^" + product_name)

        return links

    def items_per_page(self):
        item_data = []
        list_items = self.all_items()  # targetting the list of items in the page
        # tarversing each items and collecting specific data to be stored
        for item in list_items:
            # looking for tags with specific class -- to extract data
            data1 = item.find(attrs={"class": "price-was-data"})
            data = str(data1)
            new = BeautifulSoup(data, 'html.parser')
            final_price_was = new.get_text()

            data2 = item.findAll(attrs={"class": "price-current"})
            data = str(data2)
            new = BeautifulSoup(data, 'html.parser')
            data = new.get_text()
            # checking is offers included in the result
            val = (data.find("Offers"))
            if val > -1:
                splitted = data.split()
                data = splitted[0]
                splitted = data.split("[")
                current_price = splitted[1]

            else:

                splitted = data.split("[")
                data = splitted[1]
                splitted = data.split()
                current_price = splitted[0]

            data3 = item.find(attrs={"class": "price-ship"})
            data = str(data3)
            new = BeautifulSoup(data, 'html.parser')
            shipping_cost = new.get_text()

            data = item.find("a")
            # link for the product details
            product_link = str(data['href'])
            image = item.find("img")  # looking for img tag
            product_name = str(image["alt"])   # name of the product
            image = str(image["src"])

            item_data.append(product_name+"^"+product_link+"^"+image+"^"+final_price_was+"^" +
                             current_price+"^"+shipping_cost+"^")  # used ^ to sperate the string later

        return item_data

    def data_insert_db(self, data):
        connection = mysql.connector.connect(
            host="localhost", user="root", passwd="", database="neweggweb")
        cursor = connection.cursor()
        for each in data:
            final_data = each.split("^")
            sql = "INSERT INTO producttbl (name,item_link,image_link,price_was,current_price,shipping) VALUES (%s, %s,%s, %s,%s,%s)"
            val = ([final_data[0], final_data[1], final_data[2],
                    final_data[3], final_data[4], final_data[5]])
            cursor.execute(sql, val)

        connection.commit()
        connection.close()

    def create_spec_table(self, data):

        for each in data:
            splitted = each.split("^")
            URL = splitted[0]
            product_name = splitted[1]
            r = requests.get(URL)

            soup = BeautifulSoup(r.content, 'html5lib')

            page_content = soup.find("div", {"class": "row-body"})

            target = page_content.find("ul")
            features = target.get_text()  # features portion
            specs = soup.findAll(
                "table", {"class": "table-horizontal"})  # specs

            table = []
            for each in specs:
                data = str(each)
                new = BeautifulSoup(data, 'html5lib')
                table_row = new.findAll("tr")
                for each in table_row:
                    data1 = each.find("th")
                    data2 = each.find("td")
                    th_data = data1.text
                    td_data = data2.text
                    table.append(
                        str(th_data) + ":" + str(td_data)
                    )
            # change code here to turn data into a csv format
            self.one_item_data.append(
                str(self.x)+". "+"Proudct Name:" + "\n" +
                product_name + "\n" +
                "Key Features: " + "\n" +
                features + "\n" +
                "Specs: " + "\n" + "\n"
                .join(table) + "\n"
            )
            self.x = self.x+1

        with open("all_data_specs.txt", "w", encoding="UTF-8") as f:
            for row in self.one_item_data:
                f.write(row)


def crawler_function():
    new = newEggCrawler(
        "https://www.newegg.com/Desktop-Graphics-Cards/SubCategory/ID-48?Tid=7709")

    lastButton = new.lastButton()  # it will traverse till the last page

    final_data1 = []
    final_data2 = []
    page = 1
    while(page <= 2):
        if page == 1:
            URL = f"https://www.newegg.com/Desktop-Graphics-Cards/SubCategory/ID-48?Tid=7709"
        else:
            URL = f"https://www.newegg.com/Desktop-Graphics-Cards/SubCategory/ID-48/Page-{page}?Tid=7709"

        new = newEggCrawler(URL)

        data1 = new.items_per_page()
        final_data1.append(data1)

        data2 = new.crawl_to_product_link()
        final_data2.append(data2)

        print(f'Done Page:{page}')
        page = page + 1

    for each in final_data1:
        new.data_insert_db(each)

    for each in final_data2:
        new.create_spec_table(each)

    print("DONE!!!")


crawler_function()
