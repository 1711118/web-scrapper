import requests
from bs4 import BeautifulSoup
import mysql.connector
connection = mysql.connector.connect(
    host="localhost", user="root", passwd="", database="neweggweb")
cursor = connection.cursor()

URL = f"https://www.newegg.com/Desktop-Graphics-Cards/SubCategory/ID-48?Tid=7709"
r = requests.get(URL)


soup = BeautifulSoup(r.content, 'html5lib')


# finding the last page info for the crawler
button = soup.findAll("button", {"class": "btn"})

buttons_list = []
for each in button:
    buttons_list.append(str(each.contents))

main_button = []
for each in buttons_list:
    splitted = each.split("['")
    if(len(splitted)) == 2:
        for each in splitted:
            final = each.split("']")
            if final[0].isdigit() == True:
                data = int(final[0])
                type(data)
                main_button.append(data)

main_button.sort()


last_button = main_button[len(main_button)-1]

one_item_data = []


page = 1
x = 1
while(page <= 3):  # insert number of page to crawl or use last_button to crawl to last page
    if page == 1:
        URL = f"https://www.newegg.com/Desktop-Graphics-Cards/SubCategory/ID-48?Tid=7709"
    else:
        URL = f"https://www.newegg.com/Desktop-Graphics-Cards/SubCategory/ID-48/Page-{page}?Tid=7709"

    r = requests.get(URL)
    soup = BeautifulSoup(r.content, 'html5lib')

    page = page+1
    print(f'done page: {page - 1}')
    list_items = soup.findAll(
        "div", {"class": "item-cell"})  # targetting the list of items in the page

    # tarversing each items and collecting specific data to be stored
    for item in list_items:
        # looking for tags with specific class -- to extract data
        data1 = item.find(attrs={"class": "price-was-data"})
        data = str(data1)
        new = BeautifulSoup(data, 'html.parser')
        final_price_was = new.get_text()

        data2 = item.findAll(attrs={"class": "price-current"})
        data = str(data2)
        new = BeautifulSoup(data, 'html.parser')
        data = new.get_text()
        # checking is offers included in the result
        val = (data.find("Offers"))
        if val > -1:
            splitted = data.split()
            data = splitted[0]
            splitted = data.split("[")
            current_price = splitted[1]

        else:

            splitted = data.split("[")
            data = splitted[1]
            splitted = data.split()
            current_price = splitted[0]

        data3 = item.find(attrs={"class": "price-ship"})
        data = str(data3)
        new = BeautifulSoup(data, 'html.parser')
        shipping_cost = new.get_text()

        data = item.find("a")
        # link for the product details
        product_link = str(data['href'])
        image = item.find("img")  # looking for img tag
        product_name = str(image["alt"])   # name of the product
        image = str(image["src"])  # image download link

        URL = product_link  # going in the product link to extract data from that page
        r = requests.get(URL)
        product_link = product_link.strip()
        soup = BeautifulSoup(r.content, 'html5lib')

        page_content = soup.find("div", {"class": "row-body"})

        target = page_content.find("ul")
        features = target.get_text()  # features portion
        specs = soup.findAll("table", {"class": "table-horizontal"})  # specs

        table = []
        for each in specs:
            data = str(each)
            new = BeautifulSoup(data, 'html5lib')
            table_row = new.findAll("tr")
            for each in table_row:
                data1 = each.find("th")
                data2 = each.find("td")
                th_data = data1.text
                td_data = data2.text
                table.append(
                    str(th_data) + ":" + str(td_data)
                )
        # change code here to turn data into a csv format
        one_item_data.append(
            str(x)+". "+"Proudct Name:" + "\n" +
            product_name + "\n" +
            "Key Features: " + "\n" +
            features + "\n" +
            "Specs: " + "\n" + "\n"
            .join(table) + "\n"
        )

        product_name = product_name.strip()
        sql = "INSERT INTO producttbl (name,item_link,image_link,price_was,current_price,shipping) VALUES (%s, %s,%s, %s,%s,%s)"
        val = ([product_name, product_link, image,
               final_price_was, current_price, shipping_cost])
        cursor.execute(sql, val)
        x = x+1


connection.commit()
connection.close()
with open("all_data_specs.txt", "w", encoding="UTF-8") as f:
    for row in one_item_data:
        f.write(row)
