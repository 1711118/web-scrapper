import os
import requests
import datetime
from bs4 import BeautifulSoup
from requests.api import head

URL = "https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/Press-Release"
r = requests.get(URL)


soup = BeautifulSoup(r.content, 'html5lib')


def fileDownloader(URL):

    i = 1
    links = soup.find_all('a')
    for file in links:

        link = file['href']
        # pdf file downloader
        if link.endswith('.pdf'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".pdf", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link
                with open(str(i) + ".pdf", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            else:
                with open(str(i) + ".pdf", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)
        # mp3 file downloader
        elif link.endswith('.mp3'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".mp3", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link
                with open(str(i) + ".mp3", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            else:
                with open(str(i) + ".mp3", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

        # mp4 file downloader
        elif link.endswith('.mp4'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".mp4", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link
                with open(str(i) + ".mp4", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            else:
                with open(str(i) + ".mp4", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

        i = i + 1


def imageDownloader(URL, folder):

    try:
        os.mkdir(os.path.join(os.getcwd(), folder))
    except:
        pass
    os.chdir(os.path.join(os.getcwd(), folder))
    i = 1
    images = soup.find_all('img')
    for image in images:

        link = image['src']

        if link.endswith('.jpeg'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".jpeg", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link

                with open(str(i) + ".jpeg", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            else:
                with open(str(i) + ".jpeg", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

        elif link.endswith('.png'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".png", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)
            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link

                with open(str(i) + ".png", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            else:
                with open(str(i) + ".png", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

        elif link.endswith('.bmp'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".bmp", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)
            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link

                with open(str(i) + ".bmp", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

            else:
                with open(str(i) + ".bmp", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

        elif link.endswith('.jpg'):

            if link.startswith("//"):

                link = "http:"+link

                with open(str(i) + ".jpg", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)
            elif link.startswith("/"):

                link = "https://mofa.gov.bd"+link

                with open(str(i) + ".jpg", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)
            else:
                with open(str(i) + ".jpg", 'wb') as f:
                    try:
                        im = requests.get(link)
                        f.write(im.content)
                        print('Done: ', i)
                    except:
                        im = requests.get(link, verify=False)
                        f.write(im.content)
                        print('Done: ', i)

        i = i + 1


def tableCreator():

    table = soup.find("table")

    trs = table.find_all("tr")

    ct = datetime.datetime.now()
    ts = ct.timestamp()

    url = URL.split("/")

    name = str(url[2]) + "_" + str(ts) + "_" + "1711118"
    with open(f"{name}.csv", "w", encoding="UTF-8") as f:

        for tr in trs:
            tds = tr.find_all("td")
            f.write(str(tds[0].contents[0]) + ",")
            f.write(str(tds[1].contents[1].contents[0])+",")
            f.write(str(tds[2].contents[0]) + "\n")


def textCreator():

    head = soup.find('head')
    body = soup.find('body')

    ct = datetime.datetime.now()
    ts = ct.timestamp()

    url = URL.split("/")

    name = str(url[2]) + "_" + str(ts) + "_" + "1711118"
    with open(f"{name}.txt", "w", encoding="UTF-8") as f:
        f.write(str(head))
        f.write(str(body))


tableCreator()
textCreator()
imageDownloader(URL, 'New_folder')
fileDownloader(URL)
